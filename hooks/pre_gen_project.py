pypackage = '{{ cookiecutter.pypackage }}'
msg = 'Project Python name should be valid Python identifier!'
if hasattr(pypackage, 'isidentifier'):
    assert pypackage.isidentifier(), msg
