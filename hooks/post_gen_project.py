"""
Does the following:
1. Generates and saves random secret keys
2. Removes useless styles
"""
from __future__ import print_function
import os
import random
import shutil

# Get the root project directory
ROOT = os.path.realpath(os.path.curdir)
# Use the system PRNG if possible
try:
    random = random.SystemRandom()
    using_sysrandom = True
except NotImplementedError:
    using_sysrandom = False

ALLOWED_CHARS = 'abcdefghijklmnopqrstuvwxyz0123456789!@%^&*(-_=+)'
SECRET_KEY_TOKEN = 'insecure-secret-key'
DB_PASSWORD_TOKEN = 'db-password'
STYLE = '{{ cookiecutter.style }}'
STYLES = ('less', 'scss')

POSTACTIVATE_TEMPLATE = '''#!/bin/bash
# This hook is sourced after this virtualenv is activated.
export DJANGO_SETTINGS_MODULE=settings
nvm use `cat {root}/.nvmrc`
'''.format(root=ROOT)


def get_random_string(length=50):
    """
    Returns a securely generated random string.
    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits
    """
    if using_sysrandom:
        return ''.join(random.choice(ALLOWED_CHARS) for i in range(length))
    print(
        "Couldn't find a secure pseudo-random number generator on your system."
        "Please change change your secret variables in manually."
    )
    return SECRET_KEY_TOKEN


def replace_in_file(file_location, token, replacement):
    # Open locals.py
    with open(file_location) as f:
        file_ = f.read()

    # Replace "CHANGEME!!!" with SECRET_KEY_TOKEN
    file_ = file_.replace(token, replacement, 1)

    # Write the results to the locals.py module
    with open(file_location, 'w') as f:
        f.write(file_)


# Generates and saves random secret key for local settings'''
settings = os.path.join(ROOT, 'settings.py')
replace_in_file(settings, SECRET_KEY_TOKEN, get_random_string())

# Generates and saves random secret key for ansible playbook
playbook = os.path.join(ROOT, 'playbook.yml')
replace_in_file(playbook, SECRET_KEY_TOKEN, get_random_string())
replace_in_file(playbook, DB_PASSWORD_TOKEN, get_random_string(12))

# Keep only the right style
for style in STYLES:
    if style != STYLE:
        shutil.rmtree(os.path.join(ROOT, 'assets', style))
