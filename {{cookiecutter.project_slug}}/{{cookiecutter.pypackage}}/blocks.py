from django.utils.translation import ugettext_lazy as _

from wagtail.wagtailcore import blocks

from wagtail.wagtailimages.blocks import ImageChooserBlock

from wapps.mixins import ContextBlock
from wapps.utils import mark_safe_lazy


class CarouselImageBlock(blocks.StructBlock):
    image = ImageChooserBlock()

    class Meta:
        icon = 'fa-photo'
        label = _('Carousel Image')

    @property
    def url(self):
        return self.value.url


class CarouselBlock(blocks.StructBlock):
    title = blocks.CharBlock(label=_('Title'))
    description = blocks.RichTextBlock(label=_('Description'))
    images = blocks.ListBlock(CarouselImageBlock)

    class Meta:
        icon = 'fa-photo'
        template = '{{ cookiecutter.pypackage }}/blocks/carousel.html'
        label = _('Carousel')

    def get_context(self, value):
        context = super(CarouselBlock, self).get_context(value)
        context['carousel'] = value
        return context


class SubpagesBlock(ContextBlock, blocks.StructBlock):
    class Meta:
        icon = 'fa-sitemap'
        template = '{{ cookiecutter.pypackage }}/blocks/pagelist.html'
        label = _('Subpages')

    def get_context(self, value, page_context=None, **kwargs):
        context = super(SubpagesBlock, self).get_context(value)
        if page_context:
            page = page_context['page']
            context['pages'] = page.get_children().live().specific()
        return context

    def render_form(self, value, prefix='', errors=None):
        return mark_safe_lazy('''<h3 style="text-align: center; font-weight: bold;">
            <span class="icon icon-fa-sitemap"></span>
            {label}
        </h3>
        '''.format(label=_('Subpages')))


class RichTextBlock(blocks.RichTextBlock):
    class Meta:
        template = '{{ cookiecutter.pypackage }}/blocks/richtext.html'


class GalleryBlock(blocks.StructBlock):
    title = blocks.CharBlock(label=_('Title'), required=False)
    show_details = blocks.BooleanBlock(label=_('Display details'), default=False, required=False,
                                       help_text=_('Display image title and details'))
    images = blocks.ListBlock(ImageChooserBlock())

    class Meta:
        icon = 'fa-photo'
        template = '{{ cookiecutter.pypackage }}/blocks/gallery.html'
        label = _('Gallery')

    def get_context(self, value):
        context = super(GalleryBlock, self).get_context(value)
        context['gallery'] = value
        return context
