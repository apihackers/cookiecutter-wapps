from django.apps import AppConfig


class {{cookiecutter.pypackage|replace('_', '')|title}}Config(AppConfig):
    name = '{{cookiecutter.pypackage}}'
