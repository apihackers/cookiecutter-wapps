from django.db import models
from django.utils.translation import ugettext_lazy as _

from wagtail.wagtailadmin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.wagtailcore import blocks
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailcore.models import Page
from wagtail.wagtailsearch import index

from wapps.blog.models import BlogBlock
from wapps.models import StaticPage as WappsStaticPage
from wapps.utils import hide_parent_type

from .blocks import (
    CarouselBlock, GalleryBlock, SubpagesBlock, RichTextBlock
)


class StaticPageWidgets(blocks.StreamBlock):
    gallery = GalleryBlock()
    subpages = SubpagesBlock()
    richtext = RichTextBlock()


@hide_parent_type
class StaticPage(WappsStaticPage):
    widgets = StreamField(StaticPageWidgets())

    content_panels = WappsStaticPage.content_panels + [
        StreamFieldPanel('widgets'),
    ]

    class Meta:
        verbose_name = _('Static Page')


class HomeStreamBlock(blocks.StreamBlock):
    carousel = CarouselBlock()
    gallery = GalleryBlock()
    richtext = RichTextBlock()
    blog = BlogBlock()


class HomePage(Page):
    body = StreamField(HomeStreamBlock())

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    content_panels = [
        FieldPanel('title', classname="full title"),
        StreamFieldPanel('body'),
    ]

    api_fields = ('body',)

    class Meta:
        verbose_name = _('Home')
