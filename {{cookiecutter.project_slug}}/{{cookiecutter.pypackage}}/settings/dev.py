from .base import *  # noqa

DEBUG = True

INTERNAL_IPS = ['127.0.0.1', '::1']

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '::1']

for template_engine in TEMPLATES:  # noqa
    template_engine['OPTIONS']['debug'] = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEBUG_TOOLBAR_PATCH_SETTINGS = False
