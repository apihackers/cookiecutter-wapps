import '../{{ cookiecutter.style }}/base.{{ cookiecutter.style }}';

import Vue from 'vue';
import Common from './mixins/common';

new Vue({
    mixins: [Common],
    ready() {
        console.debug('Default bundle loaded');  // eslint-disable-line no-console
    }
});
