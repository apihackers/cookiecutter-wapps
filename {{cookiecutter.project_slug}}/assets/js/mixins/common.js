import Vue from 'vue';

import ScrollUp from './scrollup';
import ScrollBox from '../components/scrollbox.vue';


// Vue.use(require('../plugins/config'));
// Vue.use(require('../plugins/i18n'));
// Vue.use(require('../plugins/moment'));
// Vue.use(require('../plugins/images'));
Vue.use(require('../plugins/scroll'));
Vue.use(require('../plugins/transitions'));
// Vue.use(require('../plugins/text'));

Vue.component('scrollbox', ScrollBox);

export default {
    el: 'body',
    mixins: [ScrollUp],
};
