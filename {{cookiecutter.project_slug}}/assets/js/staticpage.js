import '../{{ cookiecutter.style }}/staticpage.{{ cookiecutter.style }}';

import Vue from 'vue';
import Common from './mixins/common';

new Vue({
    mixins: [Common],
    ready() {
        console.debug('Static page ready');  // eslint-disable-line no-console
    },
});
