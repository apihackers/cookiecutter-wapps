import Velocity from 'velocity-animate';
import u from '../utils';

export function install(Vue) {

    /**
     * Fetch an element given its selector.
     * Pass through if it's already an element
     * @param  {Element|String} target [description]
     * @return {Element}        [description]
     */
    function resolve(target) {
        if (target instanceof Element) return target;
        return document.querySelector(target);
    }

    Vue.prototype.$scrollTo = Vue.scrollTo = function(target) {
        const el = resolve(target);
        Velocity(el, 'scroll', {duration: 500});
    };

    /**
     * Animated scrolling to an anchor or an element
     */
    Vue.directive('scroll-to', {
        bind() {
            this.handler = this.scrollOnClick.bind(this);
            this.el.addEventListener('click', this.handler);
        },
        unbind() {
            this.el.removeEventListener('click', this.handler);
        },
        update(value) {
            this.target = resolve(value);
        },
        scrollOnClick(event) {
            event.preventDefault();
            Velocity(this.target, 'scroll', {duration: 500});
        }
    });

    /**
     * Animated scrolling to an anchor
     */
    Vue.directive('scrollspy', {
        twoWay: true,
        params: ['scrollspy-distance', 'scrollspy-edge'],
        bind() {
            this.handler = this.onScroll.bind(this);
            window.addEventListener('scroll', this.handler);
            window.addEventListener('resize', this.handler);
        },
        unbind() {
            window.removeEventListener('scroll', this.handler);
            window.removeEventListener('resize', this.handler);
            delete this.handler;
        },
        onScroll() {
            let position = this.el.offsetTop;
            if (this.params.scrollspyDistance) {
                position += parseInt(this.params.scrollspyDistance);
            }
            this.set(position < u.viewTop());
        }
    });


    /**
     * Apply an animate.css on scroll in viewport
     */
    Vue.directive('animate', {
        params: ['animationDelay', 'animationDuration'],
        /**
         * Attach the on_scroll binded event handler
         * and store a reference.
         */
        bind() {
            this.animation = this.expression;
            this.handler = this.on_scroll.bind(this);
            this.set_style()
            window.addEventListener('scroll', this.handler);
            window.addEventListener('resize', this.handler);
            this.vm.$on('hook:attached', this.handler);
            this.on_scroll();
        },
        /**
         * Remove event listeners
         */
        unbind() {
            if (this.handler) {
                this.el.removeEventListener('scroll', this.handler);
                delete this.handler;
            }
        },
        set_style() {
            this.el.style.visibility = 'hidden';
            if (this.params.animationDuration) {
                const duration = `${this.params.animationDuration}s`;
                this.el.style['animation-duration'] = duration;
                this.el.style['-o-animation-duration'] = duration;
                this.el.style['-ms-animation-duration'] = duration;
                this.el.style['-moz-animation-duration'] = duration;
                this.el.style['-webkit-animation-duration'] = duration;
            }
            if (this.params.animationDelay) {
                const delay = `${this.params.animationDelay}s`;
                this.el.style['animation-delay'] = delay;
                this.el.style['-o-animation-delay'] = delay;
                this.el.style['-ms-animation-delay'] = delay;
                this.el.style['-moz-animation-delay'] = delay;
                this.el.style['-webkit-animation-delay'] = delay;
            }
        },
        is_in_view() {
            const view_top = u.viewTop();
            const view_bottom = view_top + window.innerHeight;
            const el_top = this.el.offsetTop;
            return (Vue.util.inDoc(this.el)) && (el_top <= view_bottom);
        },
        on_scroll() {
            if (this.is_in_view()) {
                this.el.style.visibility = 'visible';
                this.el.classList.add('animated');
                this.el.classList.add(this.animation);
                window.removeEventListener('scroll', this.handler);
                delete this.handler;
            }
        }
    });
}
