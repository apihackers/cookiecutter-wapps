import '../../{{ cookiecutter.style }}/blocks/gallery.{{ cookiecutter.style }}';
import Baguette from 'baguettebox.js';

const CONTAINER_CLASS = 'gallery-container';

function captions(el) {
    if (el.title && el.dataset.details) {
        return `<h5>${el.title}</h5><p>${el.dataset.details}</p>`;
    } else {
        return `<h5>${el.title || el.dataset.details}</h5>`;
    }
}

export function install(Vue) {
    /**
     * Apply an animate.css on scroll in viewport
     */
    Vue.directive('gallery', {
        params: ['galleryAnimation', 'galleryDetails'],
        /**
         * Attach the on_scroll binded event handler
         * and store a reference.
         */
        bind() {
            const options = {animation: this.params.galleryAnimation || 'fadeIn'};

            if (this.params.galleryDetails !== undefined) {
                options.captions = captions;
            }

            this.el.classList.add(CONTAINER_CLASS);

            Baguette.run(`.${CONTAINER_CLASS}`, options);
        },
        /**
         * Remove event listeners
         */
        unbind() {
            Baguette.destroy();
        },
    });
}
