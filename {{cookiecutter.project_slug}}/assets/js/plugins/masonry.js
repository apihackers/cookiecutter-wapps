import Masonry from 'masonry-layout';
import imagesLoaded from 'imagesloaded';

import u from '../utils';

const ANIMATION_END_EVENTS = [
    'webkitAnimationEnd',
    'mozAnimationEnd',
    'MSAnimationEnd',
    'oanimationend',
    'animationend'
];


export function install(Vue) {
    /**
     * Apply an animate.css on scroll in viewport
     */
    Vue.directive('masonry', {
        params: ['masonrySelector', 'masonrySizer', 'masonryAnimated'],

        selector() {
            return this.params.masonrySelector || '.masonry-item';
        },

        /**
         * Attach the on_scroll binded event handler
         * and store a reference.
         */
        bind() {
            this.el.classList.add('masonry');
            this.masonry = new Masonry(this.el, {
                itemSelector: this.selector(),
                columnWidth: this.params.masonrySizer ||'.masonry-sizer',
                percentPosition: true
            });

            imagesLoaded(this.el)
                .on('progress', (instance, image) => {
                    if (this.params.masonryAnimated) {
                        const item = u.closest(image.img, this.selector());
                        const handler = function() {
                            this.masonry.layout();
                            ANIMATION_END_EVENTS.forEach(ev => item.removeEventListener(ev, handler));
                        }.bind(this);
                        ANIMATION_END_EVENTS.forEach(ev => item.addEventListener(ev, handler));
                    }
                    this.masonry.layout();
                })
                .on('always', () => {
                    this.masonry.layout();
                });
        },
        /**
         * Remove event listeners
         */
        unbind() {
            this.masonry.destroy();
        }
    });
}
