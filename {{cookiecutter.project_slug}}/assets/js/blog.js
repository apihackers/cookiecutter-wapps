import '../{{ cookiecutter.style }}/blog.{{ cookiecutter.style }}';

import Vue from 'vue';
import Common from './mixins/common';

new Vue({
    mixins: [Common],
    ready() {
        console.debug('Blog bundle loaded');  // eslint-disable-line no-console
    }
});
