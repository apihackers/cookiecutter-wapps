import '../{{ cookiecutter.style }}/home.{{ cookiecutter.style }}';

import Vue from 'vue';

import Common from './mixins/common';

new Vue({
    mixins: [Common],
    components: {},
    ready() {
        console.debug('Home ready');  // eslint-disable-line no-console
    },
});
