import '../{{ cookiecutter.style }}/gallery.{{ cookiecutter.style }}';

import Vue from 'vue';
import Common from './mixins/common';

Vue.use(require('./plugins/gallery'));
Vue.use(require('./plugins/masonry'));

new Vue({
    mixins: [Common],
    ready() {
        console.debug('Gallery page loaded')  // eslint-disable-line no-console
    }
});
