#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    try:
        import settings
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
    except:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{cookiecutter.pypackage}}.settings.dev")
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
