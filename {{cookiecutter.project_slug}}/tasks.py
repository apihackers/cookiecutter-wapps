import os
import shutil
import sys

from glob import glob
from invoke import task


ROOT = os.path.abspath(os.path.dirname(__file__))
SSH_HOST = '{{cookiecutter.deploy_server}}'
SSH_USER = '{{cookiecutter.deploy_user}}'
DB_USER = '{{cookiecutter.pypackage}}'
REMOTE_SERVER_NAME = '{{cookiecutter.server_name}}'
REMOTE_SERVER_PORT = 443
LOCAL_SERVER_NAME = 'localhost'
LOCAL_SERVER_PORT = 8000
RSYNC_ROOT = '{0}:/srv/{{cookiecutter.deploy_user}}'.format(SSH_HOST)

LANGUAGES = ['fr']
I18N_DOMAIN = 'django'


CLEAN_PATTERNS = [
    'build', 'dist', '**/*.pyc', '**/__pycache__', '.tox', '**/*.mo', 'reports'
]


def color(code):
    '''A simple ANSI color wrapper factory'''
    return lambda t: '\033[{0}{1}\033[0;m'.format(code, t)


green = color('1;32m')
red = color('1;31m')
blue = color('1;30m')
cyan = color('1;36m')
purple = color('1;35m')
white = color('1;39m')


def header(text):
    '''Display an header'''
    print(' '.join((blue('>>'), cyan(text))))
    sys.stdout.flush()


def info(text, *args, **kwargs):
    '''Display informations'''
    text = text.format(*args, **kwargs)
    print(' '.join((purple('>>>'), text)))
    sys.stdout.flush()


def success(text):
    '''Display a success message'''
    print(' '.join((green('✔'), white(text))))
    sys.stdout.flush()


def error(text):
    '''Display an error message'''
    print(red('✘ {0}'.format(text)))
    sys.stdout.flush()


def exit(text=None, code=-1):
    if text:
        error(text)
    sys.exit(-1)


@task
def clean(ctx):
    '''Cleanup all build artifacts'''
    header(clean.__doc__)
    with ctx.cd(ROOT):
        for pattern in CLEAN_PATTERNS:
            info(pattern)
            ctx.run('rm -rf {0}'.format(' '.join(CLEAN_PATTERNS)))


@task
def build(ctx):
    '''Build assets'''
    header('Build assets assets')
    with ctx.cd(ROOT):
        ctx.run('npm run build', pty=True)


@task
def watch(ctx):
    '''Continously build assets'''
    header('Watch assets')
    with ctx.cd(ROOT):
        ctx.run('npm run dev', pty=True)


@task
def migrate(ctx):
    '''Migrate database'''
    header('Migrate database')
    with ctx.cd(ROOT):
        ctx.run('./manage.py migrate', pty=True)


@task
def test(ctx, report=False, verbose=False):
    '''Run tests suite'''
    header(test.__doc__)
    cmd = ['pytest']
    if verbose:
        cmd.append('-v')
    if report:
        cmd.append('--junitxml=reports/tests.xml')
    with ctx.cd(ROOT):
        ctx.run(' '.join(cmd), pty=True)


@task
def cover(ctx, report=False, verbose=False):
    '''Run tests suite with coverage'''
    header(cover.__doc__)
    cmd = [
        'pytest',
        '--cov-config coverage.rc',
        '--cov-report term',
        '--cov={{cookiecutter.pypackage}}',
    ]
    if verbose:
        cmd.append('-v')
    if report:
        cmd += [
            '--cov-report html:reports/coverage',
            '--cov-report xml:reports/coverage.xml',
            '--junitxml=reports/tests.xml'
        ]
    with ctx.cd(ROOT):
        ctx.run(' '.join(cmd), pty=True)


@task
def qa(ctx):
    '''Run a quality report'''
    header(qa.__doc__)
    with ctx.cd(ROOT):
        info('Python Static Analysis')
        flake8_results = ctx.run('flake8 {{cookiecutter.pypackage}} tests', pty=True, warn=True)
        if flake8_results.failed:
            error('There is some Python lints to fix')
        else:
            success('Python code seems OK')
        info('Ensure PyPI can render README and CHANGELOG')
        readme_results = ctx.run('python setup.py check -r -s', pty=True, warn=True, hide=True)
        if readme_results.failed:
            print(readme_results.stdout)
            error('README and/or CHANGELOG is not renderable by PyPI')
        else:
            success('README and CHANGELOG are renderable by PyPI')
    if flake8_results.failed or readme_results.failed:
        exit('Quality check failed', flake8_results.return_code or readme_results.return_code)
    success('Quality check OK')


@task
def migration(ctx, app='sublime', name=None, empty=False):
    '''Create a new migration'''
    header('Create a new django migration')
    cmd = ['./manage.py', 'makemigrations', app]
    if name:
        cmd += ['-n', name]
    if empty:
        cmd.append('--empty')
    with ctx.cd(ROOT):
        ctx.run(' '.join(cmd), pty=True)


@task
def update(ctx):
    '''Update dependancies'''
    header(update.__doc__)
    with ctx.cd(ROOT):
        ctx.run('pip install -r requirements/develop.pip')
        ctx.run('npm install')


@task
def serve(ctx):
    '''Run Django dev server'''
    header('Run Django dev server')
    with ctx.cd(ROOT):
        ctx.run('./manage.py runserver', pty=True)


@task
def i18n(ctx):
    '''Extract translatable strings'''
    header(i18n.__doc__)
    with ctx.cd(ROOT):
        ctx.run('mkdir -p sublime/locale')
        ctx.run('pybabel extract -F babel.cfg -o {{cookiecutter.pypackage}}/locale/{domain}.pot {{cookiecutter.pypackage}}'.format(domain=I18N_DOMAIN))
        for lang in LANGUAGES:
            translation = os.path.join(ROOT, '{{cookiecutter.pypackage}}', 'locale', lang, 'LC_MESSAGES', '{0}.po'.format(I18N_DOMAIN))
            if not os.path.exists(translation):
                ctx.run('pybabel init -D {domain} -i {{cookiecutter.pypackage}}/locale/django.pot -d {{cookiecutter.pypackage}}/locale -l {lang}'.format(
                    lang=lang, domain=I18N_DOMAIN
                ))
        ctx.run('pybabel update -D {domain} -i {{cookiecutter.pypackage}}/locale/{domain}.pot -d {{cookiecutter.pypackage}}/locale'.format(domain=I18N_DOMAIN))
        ctx.run('rm {{cookiecutter.pypackage}}/locale/{domain}.pot'.format(domain=I18N_DOMAIN))
        success('Updated translations')


@task
def i18nc(ctx):
    '''Compile translations'''
    header(i18nc.__doc__)
    with ctx.cd(ROOT):
        ctx.run('pybabel compile -D {domain} -d {{cookiecutter.pypackage}}/locale --statistics'.format(domain=I18N_DOMAIN))
    success('Compiled translations')


@task(clean, i18nc)
def dist(ctx, buildno=None):
    '''Package for distribution'''
    header(dist.__doc__)
    cmd = ['python3 setup.py']
    if buildno:
        cmd.append('egg_info -b {0}'.format(buildno))
    cmd.append('bdist_wheel')
    with ctx.cd(ROOT):
        ctx.run(' '.join(cmd), pty=True)


@task(build, migrate, i18nc)
def bootstrap(ctx):
    '''Kickstart the initial setup'''
    header('Create a super user')
    with ctx.cd(ROOT):
        ctx.run('./manage.py createsuperuser', pty=True)


@task
def backup(ctx):
    '''Backup remote database'''
    header('Backup remote database')
    ctx.run('ssh {user}@{host} "mkdir -p backups"'.format(
        host=SSH_HOST,
        user=SSH_USER,
    ))
    ctx.run('ssh {user}@{host} "pg_dump {db} > backups/{db}-{host}-`date +%F-%H%M%S`.psql"'.format(
        host=SSH_HOST,
        user=SSH_USER,
        db=DB_USER,
    ))


@task
def fetch(ctx):
    '''Synchronize remote data'''
    header('Synchronize remote data')
    with ctx.cd(ROOT):
        ctx.run('rsync -avz --progress {0}/public/media/ media/'.format(RSYNC_ROOT))
        ctx.run('rsync -avz --progress {0}/backups/ backups/'.format(RSYNC_ROOT))


@task
def reload(ctx, detach=False):
    '''Reload database data from latest dump'''
    last_dump = last_dump_filename()
    if not last_dump:
        return
    header('Using dump {0} as initial docker-compose data'.format(last_dump))
    shutil.copy(os.path.join(ROOT, 'backups', last_dump),
    os.path.join(ROOT, 'data.sql'))
    with ctx.cd(ROOT):
        ctx.run('sed -i "s/{remote_name}\t{remote_port}/{local_name}\t{local_port}/" data.sql'.format(
            remote_name=REMOTE_SERVER_NAME,
            remote_port=REMOTE_SERVER_PORT,
            local_name=LOCAL_SERVER_NAME,
            local_port=LOCAL_SERVER_PORT
        ))
        ctx.run(' && '.join([
            'docker-compose stop',
            'docker-compose rm --force -v',
            'docker-compose up {0}'.format('-d' if detach else ''),
        ]))


def last_dump_filename():
    prefix = '{db}-{host}-'.format(db=SSH_USER, host=SSH_HOST)
    pattern = os.path.join('backups', '{0}*.psql'.format(prefix))
    versions = []
    for filename in glob(pattern):
        basename = os.path.basename(filename)
        version = basename.replace(prefix, '').replace('.psql', '')
        versions.append(version)
    if not versions:
        return
    version = sorted(versions, reverse=True)[0]
    return '{prefix}{version}.psql'.format(prefix=prefix, version=version)


@task
def fixturize(ctx):
    cmd = (
        './manage.py dumpdata --natural-foreign --natural-primary',
        '-e contenttypes -e auth.Permission -e sessions -e admin',
        '> tests/site.json'
    )
    with ctx.cd(ROOT):
        ctx.run(' '.join(cmd), pty=True)
