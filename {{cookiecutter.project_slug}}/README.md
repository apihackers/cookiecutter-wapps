# {{cookiecutter.project_name}}

[![pipeline status](https://gitlab.com/apihackers/{{cookiecutter.project_slug}}/badges/master/pipeline.svg)](https://gitlab.com/apihackers/{{cookiecutter.project_slug}}/pipelines)
[![coverage report](https://gitlab.com/apihackers/{{cookiecutter.project_slug}}/badges/master/coverage.svg)](https://gitlab.com/apihackers/{{cookiecutter.project_slug}}/commits/master)

{{cookiecutter.description}}

## Requirements

## Getting started
