const webpack = require('webpack');
const path = require('path');
const BundleTracker = require('webpack-bundle-tracker');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const css_loader = ExtractTextPlugin.extract('style', 'css?sourceMap');
const less_loader = ExtractTextPlugin.extract('style', 'css?sourceMap!less?sourceMap=source-map-less-inline');

const languages = ['fr', 'en'];

module.exports = {
    entry: {
        default: 'js/default',
        home: 'js/home',
        staticpage: 'js/staticpage',
        gallery: 'js/gallery',
        blog: 'js/blog',
    },
    output: {
        path: path.resolve('./{{cookiecutter.pypackage}}/static'),
        publicPath: '/static/',
        filename: 'js/[name]-[hash].js',
        chunkFilename: 'js/chunk.[id].js'
    },
    resolve: {
        root: path.resolve('./assets'),
        alias: {
            locale: '{{cookiecutter.pypackage}}/locale',
        },
    },
    module: {
        loaders: [
            {test: /\.vue$/, loader: 'vue'},
            {test: /\.js$/, exclude: /node_modules/, loader: 'babel?presets[]=es2015'},
            {test: /\.less$/, loader: less_loader},
            {test: /\.css$/, loader: css_loader},
            {test: /\.json$/, loader: 'json'},
            {test: /\.(jpg|jpeg|png|gif|GIF)$/, loader: 'file?name=images/[name].[ext]?[hash]'},
            {test: /images\/.*\.svg(\?.*)?$/, loader: 'file?name=images/[name].[ext]?[hash]'},
            {test: /\.woff(2)?(\?.*)?/, loader: 'file?limit=10000&name=fonts/[name].[ext]?[hash]'},
            {test: /\.(ttf|eot|otf)(\?.*)?$/, loader: 'file?name=fonts/[name].[ext]?[hash]'},
            {test: /fonts\/.*\.svg(\?.*)?$/, loader: 'file?name=fonts/[name].[ext]?[hash]'},
            // {test: /\.(otf|eot|svg|ttf|woff|woff2)(\?.+)$/, loader: 'file-loader?name=[name].[ext]'},
        ]
    },
    vue: {
        loaders: {
            css: css_loader,
            less: less_loader,
        }
    },
    plugins: [
        new webpack.ContextReplacementPlugin(/moment\/locale$/, new RegExp('^' + languages.join('|') + '$')),
        new BundleTracker({filename: `./{{cookiecutter.pypackage}}/webpack-stats.json`}),
        new ExtractTextPlugin('css/[name].[contenthash].css', {
            allChunks: true
        }),
        new webpack.optimize.CommonsChunkPlugin('common', 'cs/common.[hash].js')
    ]
};

if (process.env.NODE_ENV === 'production') {
    module.exports.plugins.push(
        // new webpack.DefinePlugin({
        //     'process.env': {
        //         NODE_ENV: 'production'
        //     }
        // }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new webpack.optimize.OccurenceOrderPlugin()
    );
} else {
    module.exports.devtool = '#source-map';
}
