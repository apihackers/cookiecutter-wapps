from sublime.settings.base import *  # noqa

DEBUG = False
GOOGLE_API_KEY = 'some-fake-api-key'

DATABASES = {
    "default": {
        # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        # DB name or path to database file if using sqlite3.
        'NAME': '{{cookiecutter.pypackage}}',
        # Not used with sqlite3.
        'USER': '{{cookiecutter.pypackage}}',
        # Not used with sqlite3.
        'PASSWORD': '{{cookiecutter.pypackage}}',
        # # Set to empty string for localhost. Not used with sqlite3.
        'HOST': 'postgres',
        # # Set to empty string for default. Not used with sqlite3.
        # "PORT": "",
    }
}

ALLOWED_HOSTS = ['*']

SECRET_KEY = 'insecure'
