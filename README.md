# Cookie Cutter for API Hackers Wagtail/Wapps sites quickstart

A [cookiecutter][] template for [API Hacker][apihackers] [Wagtail][] web site.


## Features

A [Django][] project with [Wagtail][] pre-installed:

* Django 1.10 and Wagtail 1.7
* Dev tasks with invoke
* Works with Python 3.x

## Getting Started

Here is how we create a new Django project quickly while letting [cookiecutter][] to do all the work.

To get started we assume the following dependencies :

* pip
* virtualenvwrapper
* nvm
* Docker/Docker Compose
* cookiecutter


Now run it against this repo:

```shell
$ cookiecutter https://gitlab.com/apihackers/cookiecutter-wapps.git
```

You'll be prompted for some values. Provide them, then cookiecutter will perform the follwing :

* generate the project skeleton
* generate a secret key
* create and activate the virtualenv
* go to the project directory and set the virtualenvwrapper project
* install the python dependencies
* install the npm dependencies
* initialize the git repository
* perform the initial commit
* associate the remote repository and push to it

You can then start developping. An invoke task helper is present:

```shell
$ inv -l
Available tasks:

  backup       Backup remote database
  build        Build assets
  clean        Cleanup all build artifacts
  dronecrypt   Encrypt drone secrets
  fetch        Synchronize remote data
  i18n         Update translations
  i18nc        Compile translations
  migrate      Migrate database
  migration    Create a new migration
  serve        Run Django dev server
  watch        Continously build assets
```

To get started, perform the following:

```shell
$ inv build migrate serve
```

You can access your site at <http://localhost:8000>.
The Admin back-end is available at <http://localhost:8000/admin/>.


[apihackers]: https://apihackers.com
[cookiecutter]: https://github.com/audreyr/cookiecutter
[Wagtail]: https://wagtail.io
[Django]: https://www.djangoproject.com/
